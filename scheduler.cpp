#include "scheduler.h"

Scheduler::Scheduler()
{
    timer = new QTimer();
    timer->setInterval(60*1000);
    connect(timer,SIGNAL(timeout()), this, SLOT(checkCommands()));
}
//парсинг JSON расписания
void Scheduler::parseSchedule(QString scheduleJson) {
    QJsonParseError parseError;
    QJsonDocument doc = QJsonDocument::fromJson(scheduleJson.toUtf8(), &parseError);
    if(parseError.error!= QJsonParseError::NoError) {
        //если произошла ошибка - инициируем сигнал и прекращаем дальнейший разбор
        emit scheduleParseFailed(parseError.errorString());;
        return;
    }
    QJsonObject jObject = doc.object();
    QVariantMap mainMap = jObject.toVariantMap();
    QRegExp dateRegExp("^\\d{4}-\\d{1,2}-\\d{1,2}$");
    for(QVariantMap::const_iterator iter = mainMap.begin(); iter != mainMap.end(); ++iter) {
        QString key=iter.key();
        QDateTime date;
        int dayOfWeek = 0;
        //определяем тип элемента расписания
        Enums::ScheduleType type = Enums::EVERYDAY;
        if(dateRegExp.exactMatch(key)) {
            type= Enums::DATE;
            date = QDateTime::fromString(key, "yyyy-MM-dd");
        }
        else {
            if(DaysOfWeeks.contains(key)) {
                type=Enums::DAY_OF_WEEK;
                dayOfWeek = DaysOfWeeks.indexOf(key)+1;
            }
            else {
                if(key != "Everyday")
                    //вхождение не распознано, пропускаем
                    continue;
            }
        };
        QList<QVariant> list = iter.value().toList();
        QListIterator<QVariant> listIter(list);
        while(listIter.hasNext()) {
            QVariant itemVariant = listIter.next();
            //разбираем конкретное задание
            ScheduleItem item = parseScheduleItem(itemVariant.toMap());
            if(item.isParsed) {
                //устанавливаем время команды
                item.scheduleType = type;
                if(item.scheduleType == Enums::DATE) {
                    QDateTime itemDate(date);
                    itemDate.setTime(item.time);
                    item.dateTime = itemDate;
                }
                item.dayOfWeek = dayOfWeek;
                scheduleList.append(item);
            }
        }
    }
    //сортируем полное расписание
    std::sort(scheduleList.begin(), scheduleList.end(), ScheduleItem_lessThan());
}

//разбираем конкретное задание
ScheduleItem Scheduler::parseScheduleItem(QMap<QString, QVariant> entry) {
    ScheduleItem item;
    item.isParsed = true;
    for(QVariantMap::const_iterator iter = entry.begin(); iter != entry.end(); ++iter) {
        if(iter.key() == "id")
            item.id = iter.value().toDouble();
        if(iter.key()=="time") {
            item.time=QTime::fromString(iter.value().toString(), "H:mm");
        }
        if(iter.key()=="type") {
            QMetaEnum metaEnum = QMetaEnum::fromType<Enums::TaskType>();
            int index = metaEnum.keyToValue(iter.value().toString().toStdString().c_str());
            if(index < 0) {
                //команда не распознана, пропускаем
                item.isParsed = false;
                break;
            }
            item.type = static_cast<Enums::TaskType>(index);
        }
        if(iter.key()=="params") {
            QList<ScheduleParam> scheduleParams;
            QMap<QString, QVariant> paramsMap = iter.value().toMap();
            for(QVariantMap::const_iterator paramIter = paramsMap.begin(); paramIter != paramsMap.end(); ++paramIter) {
                ScheduleParam param;
                param.name = paramIter.key();
                param.value = paramIter.value();
                scheduleParams.append(param);
            }
            item.params=scheduleParams;
        }
    }
    return item;
}

bool Scheduler::duplicateTaskExists(ScheduleItem item) {
    bool found=false;
    QDateTime itemDate = getNextLaunchTime(item);
    //т.к. полный список, из которого формируется пул, уже отсортирован по времени и приоритету - можем проверять только пул по мере его заполнения.
    QListIterator<ScheduleItem> it(pool);
    while(it.hasNext() && !found) {
        ScheduleItem currentItem = it.next();
        if(currentItem.type==item.type) {
            QDateTime currentItemDate = getNextLaunchTime(currentItem);
            found = itemDate == currentItemDate;
        }
    }

    return found;

}

//отбор пула заданий на день
void Scheduler::selectDailyPool() {
    QDateTime today=QDateTime::currentDateTime();
    QTime nowTime = today.time();
    pool.clear();
    QListIterator<ScheduleItem> scheduleIterator(scheduleList);
    while(scheduleIterator.hasNext()) {
        ScheduleItem currentItem = scheduleIterator.next();
        switch(currentItem.scheduleType) {
        //отбор с точностью до минуты
        case Enums::DATE:
            //добавляем только если время задания не истекло и в пуле нет дублирующих заданий
            if(currentItem.dateTime.date() == today.date() && (currentItem.time.hour() > nowTime.hour() || currentItem.time.hour()==nowTime.hour() && currentItem.time.minute() >= nowTime.minute()))
                if(!duplicateTaskExists(currentItem))
                    pool.append(currentItem);
            break;
        case Enums::DAY_OF_WEEK:
            if(currentItem.dayOfWeek==today.date().dayOfWeek() && (currentItem.time.hour() > nowTime.hour() || currentItem.time.hour()==nowTime.hour() && currentItem.time.minute() >= nowTime.minute()))
                if(!duplicateTaskExists(currentItem))
                    pool.append(currentItem);
            break;
        case Enums::EVERYDAY:
            if(currentItem.time.hour() > nowTime.hour() || currentItem.time.hour()==nowTime.hour() && currentItem.time.minute() >= nowTime.minute())
                if(!duplicateTaskExists(currentItem))
                    pool.append(currentItem);
        }
    }
    std::sort(pool.begin(), pool.end(), ScheduleItem_lessThanDaily());
    dailyPoolReady(pool);
}


//парсим расписание, выбираем пул на день и запускаем
void Scheduler::loadSchedule(QString scheduleJson) {
    this->parseSchedule(scheduleJson);
    isScheduleLoaded = scheduleList.length()>0;
    selectDailyPool();
    startSchedule();
}

//выполнение заданий
void Scheduler::checkCommands(){
    QDateTime now = QDateTime:: currentDateTime();
    QTime nowTime = now.time();
    //если робот не включен
    if(!isPowerOn) {
        //проверяем, можно ли включить робота
        if(now.date()==powerOnTime.date() && nowTime.hour()==powerOnTime.time().hour() && nowTime.minute()==powerOnTime.time().minute()) {
            onRobotPowerOn();
            return;
        }
    } else {
        //если полночь - пул перезагружаем
        if(now.time().hour()==0 && now.time().minute()==0)
            selectDailyPool();
        //проходим по cgbcку событий и выбираем события для этого времени
        bool powerOffFound = false;
        bool powerOnTimeFound = false;
        QListIterator<ScheduleItem> it(scheduleList);
        while(it.hasNext()) {
            ScheduleItem currentTask = it.next();
            //проверяем, пришло ли время исполнения задания и включен ли робот
            if(currentTask.time.hour()==nowTime.hour() && currentTask.time.minute()==nowTime.minute() && isPowerOn) {
                //если задание на выключение - ищем время, когда включить
                if(currentTask.type==Enums::power_off) {
                    powerOffFound = true;
                    powerOnTime = QDateTime::currentDateTime();
                    QDateTime tempTime = QDateTime::currentDateTime();
                    QListIterator<ScheduleItem> listIterator(scheduleList);
                    while(listIterator.hasNext()) {
                        ScheduleItem item = listIterator.next();
                        if(item.type == Enums::power_on) {
                            tempTime = getNextLaunchTime(item);
                            if(!powerOnTimeFound)
                                powerOnTime = tempTime;
                            powerOnTimeFound = true;
                            if(tempTime < powerOnTime)
                                powerOnTime = tempTime;
                        }
                    }
                    //отрабатываем отключение
                    if(powerOffFound)
                        onRobotPowerOff();
                    //сообщаем, найдено ли время включения
                    if(powerOnTimeFound)
                        emit powerOnFound(powerOnTime);
                    else {
                        emit powerOnNotFound();
                    }
                }
                //выполняем задание
                emit taskExecuted(currentTask);
            }
        }
    }
}

void Scheduler::onRobotPowerOn() {
    isPowerOn = true;
    //выбираем события с текущего момента
    selectDailyPool();
    startSchedule();
}

void Scheduler::onRobotPowerOff() {
    isPowerOn = false;
}

void Scheduler::startSchedule(){
    //проверяем, нужно ли что-то выполнять
    checkCommands();
    //запускаем таймер
    timer->start();
}

void Scheduler::stopSchedule(){
    timer->stop();
}

QDateTime Scheduler::getNextLaunchTime(ScheduleItem item) {
    QDateTime time;
    QDateTime now = QDateTime::currentDateTime();
    QTime nowTime = now.time();
    switch(item.scheduleType) {
    case Enums::DATE:
        time = item.dateTime;
        break;
    case Enums::DAY_OF_WEEK: {
        int diff = now.date().dayOfWeek()-item.dayOfWeek;
        if(diff < 0) {
            diff = 7+diff;
        } else {
            if(diff==0) {
                diff = item.time < nowTime ? 7:0;
            }
        }
        time = now.addDays(diff);
        time.setTime(item.time);
    }
        break;
    case Enums::EVERYDAY: {
        if(item.time<nowTime)
            time = now.addDays(1);
        else {
            time=now;
        }
        time.setTime(item.time);
    }
        break;
    }

    return time;
}

Scheduler::~Scheduler()
{
    timer->stop();
    delete timer;
    scheduleList.clear();
    pool.clear();
}
