#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->browseButton,SIGNAL(clicked()),this,SLOT(selectFile()));
    connect(ui->loadButton, SIGNAL(clicked()),this,SLOT(loadSchedule()));
    connect(ui->stopButton,SIGNAL(clicked()), this, SLOT(stopSchedule()));

    scheduler = new Scheduler();
    connect(scheduler, SIGNAL(taskExecuted(ScheduleItem)),this,SLOT(onTaskExecuted(ScheduleItem)));
    connect(scheduler, SIGNAL(powerOnFound(QDateTime)), this, SLOT(onPowerOnFound(QDateTime)));
    connect(scheduler, SIGNAL(powerOnNotFound()), this, SLOT(onPowerOnNotFound()));
    connect(scheduler, SIGNAL(scheduleParseFailed(QString)), this, SLOT(onParseError(QString)));
    connect(scheduler, SIGNAL(dailyPoolReady(QList<ScheduleItem>)), this, SLOT(onDailyPoolReady(QList<ScheduleItem>)));
}

void MainWindow::selectFile(){
    QString filePath = QFileDialog::getOpenFileName(nullptr,"Открыть файл расписания","","*.json");
    if(filePath !=nullptr) {
        ui->filePathInput->setText(filePath);
        ui->loadButton->setEnabled(!isScheduleRunning);
        ui->stopButton->setEnabled(isScheduleRunning);
    }
}

void MainWindow::loadSchedule(){
    //если уже есть загруженное расписание - запускаем его, иначе загружаем из указанного файла
    if(!(scheduler->isScheduleLoaded) && (ui->filePathInput->text().length() > 0)) {
        QFile file(ui->filePathInput->text());
        if(!file.open(QIODevice::ReadOnly)) {
            QMessageBox::warning(nullptr,"Ошибка чтения файла",file.errorString());
        }
        log("Чтение файла расписания");
        QTextStream in(&file);
        QString fileContent = "";
        while(!in.atEnd()) {
            QString line = in.readLine();
            fileContent.append(line);
        }
        scheduler->loadSchedule(fileContent);
    } else {
        if(scheduler->isScheduleLoaded)
            scheduler->startSchedule();
        else{
            QMessageBox::warning(this, "Нет доступного расписания", "Не загружено расписание и не указан файл, который его содержит");
        }
    }
    this->isScheduleRunning = true;
    ui->loadButton->setEnabled(!isScheduleRunning);
    ui->stopButton->setEnabled(isScheduleRunning);
    //загружаем расписание и сразу выполняем

}

void MainWindow::stopSchedule() {
    this->isScheduleRunning = false;
    scheduler->stopSchedule();
    ui->loadButton->setEnabled(!isScheduleRunning);
    ui->stopButton->setEnabled(isScheduleRunning);
}

QString MainWindow::getTaskString(ScheduleItem task){
    QMetaEnum metaEnum = QMetaEnum::fromType<Enums::TaskType>();
    QString logString = QString(metaEnum.valueToKey(task.type));
    if(task.params.length()>0) {
        QListIterator<ScheduleParam> paramIterator(task.params);
        while(paramIterator.hasNext()) {
            ScheduleParam currentParam = paramIterator.next();
            logString+=" "+currentParam.name+": "+currentParam.value.toString();
        }
    }

    return logString;
}

void MainWindow::onTaskExecuted(ScheduleItem task) {
    log("Запущено задание: "+getTaskString(task));
}

void MainWindow::onPowerOnFound(QDateTime powerOnTime) {
    log("Робот будет включен "+powerOnTime.toString("dd.MM.yyyy HH:mm"));
}

void MainWindow::onPowerOnNotFound() {
    log("Не найдено задание на включение робота. Потребуется ручное включение");
}

void MainWindow::log(QString logString) {
    QDateTime now = QDateTime::currentDateTime();
    QString resultString = now.toString("dd.MM.yyyy HH:mm")+"  "+logString+"\n";
    ui->logArea->append(resultString);

}

void MainWindow::onDailyPoolReady(QList<ScheduleItem> list){
    log("Сформирован дневной пул заданий");
    QListIterator<ScheduleItem> it(list);
    while(it.hasNext()) {
        ScheduleItem currentItem = it.next();
        ui->logArea->append("    "+currentItem.time.toString("HH:mm")+": "+getTaskString(currentItem));
    }
}

void MainWindow::onParseError(QString errorString) {
    log("!!! Ошибка чтения расписания из файла: "+errorString);
    isScheduleRunning = false;
    ui->loadButton->setEnabled(!isScheduleRunning);
    ui->stopButton->setEnabled(isScheduleRunning);
}

MainWindow::~MainWindow()
{
    delete ui;
}
