#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QTextBrowser>
#include <QLineEdit>
#include <QFileDialog>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <scheduler.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private slots:
    //выбор файла с расписанием
    void selectFile();
    //загрузка и запуск расписания
    void loadSchedule();
    //остановка работы по расписанию
    void stopSchedule();
    //лог выполнения задания
    void onTaskExecuted(ScheduleItem task);
    //обработчик события "выключение, найдено время дальнейшего включения"
    void onPowerOnFound(QDateTime powerOnTime);
    //обработчик события "выключение, НЕ найдено время дальнейшего включения"
    void onPowerOnNotFound();
    //ошибка парсинга расписания
    void onParseError(QString errorString);
    //вывод дневного пула заданий на экран
    void onDailyPoolReady(QList<ScheduleItem> list);
private:
    Ui::MainWindow *ui;
    QPushButton *browseButton;
    QLineEdit *filePathInput;

    //Планировщик заданий
    Scheduler *scheduler;
    //флаг, показывает, работает ли планировщик
    bool isScheduleRunning = false;
    //логирование событий на экране
    void log(QString logString);
    //формирование строки с описанием задания
    QString getTaskString(ScheduleItem task);

};

#endif // MAINWINDOW_H
