#ifndef SCHEDULER_H
#define SCHEDULER_H
#include <QTimer>
#include <QVector>
#include <QString>
#include "entity.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QMetaEnum>

class Scheduler: public QObject
{
    Q_OBJECT
public:
    Scheduler();
    //загрузка, парсинг и запуск расписания
    void loadSchedule(QString scheduleJson);
    //запуск расписания
    void startSchedule();
    //остановка планировщика
    void stopSchedule();
    //флаг - загружено ли расписание
    bool isScheduleLoaded = false;
    ~Scheduler();
private:
    //полное расписание
    QList<ScheduleItem> scheduleList;
    //пул заданий на день
    QList<ScheduleItem> pool;
    //парсинг расписания
    void parseSchedule(QString scheduleJson);
    //отбор заданий на день
    void selectDailyPool();
    //разбор отдельного задания
    ScheduleItem parseScheduleItem(QMap<QString, QVariant> entry);
    //служебный список  - дни недели
    const QList<QString> DaysOfWeeks = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday","Saturday","Sunday"};
    //таймер
    QTimer* timer;
    //поиск дублирующего задания
    bool duplicateTaskExists(ScheduleItem item);
    //формируем дату следующего запуска задания
    QDateTime getNextLaunchTime(ScheduleItem item);
    //ближайшее время включения
    QDateTime powerOnTime;
    //флаг. Показывает, включен ли робот
    bool isPowerOn = true;
private slots:
    //поиск заданий на выполнение, выполняется периодически
    void checkCommands();
public slots:
    //выключение робота
    void onRobotPowerOn();
    //включение робота
    void onRobotPowerOff();
signals:
    //задание выполнено (для лога)
    void taskExecuted(ScheduleItem task);
    //робот выключается, время включения найдено
    void powerOnFound(QDateTime powerOnTime);
    //робот выключается, время включения не найдено, требуется ручной запуск
    void powerOnNotFound();
    //ошибка при парсинге расписания
    void scheduleParseFailed(QString error);
    //пул заданий на день сформирован, для вывода в лог
    void dailyPoolReady(QList<ScheduleItem> list);
};

#endif // SCHEDULER_H
