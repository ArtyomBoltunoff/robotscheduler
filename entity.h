#ifndef ENTITY_H
#define ENTITY_H
#include <QDateTime>
#include <QVariant>

class Enums {
public:
    //перечисление - типы элементов расписания
    enum ScheduleType {DATE, DAY_OF_WEEK, EVERYDAY} ;
    //перечисление - типы заданий
    enum TaskType {power_on, movement_mode,charger_station, launch, power_off};
    Q_ENUM(TaskType)
    Q_GADGET
    Enums() = delete;
};

//параметр задания
struct ScheduleParam {
    QString name;
    QVariant value;
    ScheduleParam(){}
};
//элемент расписания
struct ScheduleItem
{
    //дата и время запуска, для заданий с конкретной датой
    QDateTime dateTime;
    //время запуска, для ежедневных и еженедельных заданий
    QTime time;
    //день недели - для еженедельных заданий
    int dayOfWeek;
    int id;
    Enums::TaskType type;
    Enums::ScheduleType scheduleType;
    QList<ScheduleParam> params;
    //флаг "задание разобрано успешно"
    bool isParsed;
    ScheduleItem() {}
};

//используется для сортировки списка заданий, по приоритету, времени и типу заданий
struct ScheduleItem_lessThan {
    bool operator()(ScheduleItem & a, ScheduleItem & b) {
        if(a.scheduleType == b.scheduleType) {
            switch(a.scheduleType) {
            case Enums::DATE:
                if(a.dateTime != b.dateTime)
                    return a.dateTime < b.dateTime;
                else {
                    return a.type < b.type;
                }
                break;
            case Enums::EVERYDAY:
                if(a.time!=b.time)
                    return a.time < b.time;
                else {
                    return a.type < b.type;
                }
                break;
            case Enums::DAY_OF_WEEK:
                if(a.dayOfWeek == b.dayOfWeek)
                    if(a.time!=b.time)
                        return a.time < b.time;
                    else {
                        return a.type < b.type;
                    }
                else {
                    return a.dayOfWeek < b.dayOfWeek;
                }
                break;
            }
        } else {
            return a.scheduleType < b.scheduleType;
        }
    }
};
//для сортировки пула событий на день
struct ScheduleItem_lessThanDaily {
    bool operator()(ScheduleItem & a, ScheduleItem & b) {
        if(a.time == b.time)
            return a.scheduleType < b.scheduleType;
        else {
            return a.time < b.time;
        }
    }
};

#endif // ENTITY_H
